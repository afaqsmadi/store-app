var mysql = require('mysql');
var connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'casher-Transactions'
});
//Connect
connection.connect(function() {
	console.log("Database connected");
});

//to retrive all user
module.exports.findAll = function(callback) {
	connection.query("SELECT * FROM casher", callback);
}
//to sign up
module.exports.register=function(req,res){
    var data=req.body
    console.log("data",data)
    connection.query('INSERT INTO cashers SET ?',data, function (error, results, fields) {
      console.log("results",results)
      if (error) {
        res.json({
            status:false,
            message:error
        })
      }else{
          res.json({
            status:true,
            data:results,
            message:'user registered sucessfully'
        })
      }
    });
}

//Select cashier name “Dropdown list”

module.exports.findByName = function(req, res) {
  connection.query("SELECT firstName FROM cashers", function (err, result, fields) {
      if (err) throw err;
      res.json(result);
    });
}

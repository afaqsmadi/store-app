var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var mysql = require('mysql');
var apiCasher=require('./api/casher')
var apiTransactions=require("./api/transactions")
var apiJoin=require("./api/join");

// Initialize Express App
var app = express();
// view engine setup
app.set('views', path.join(__dirname, '../client/views'));
app.set('view engine', 'hbs');

// Use Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Set Static Path
app.use(express.static('../client/public/js/app'));

// // Import API Routes
// app.use('/allCasher',apiCasher);
app.use('/oneCasher',apiCasher);
app.use('/apiTransactions',apiTransactions);
app.use('/',apiJoin);

port = process.env.PORT || 3000;

app.listen(port, function() {
	console.log("listening to port " + port);
})


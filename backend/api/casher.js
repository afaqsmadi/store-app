var express = require('express');
var app = express();

// Import User Module Containing Functions Related To User Data
var casher = require('../models/casher');

// API Routes
app.get('/', function(req, res) {
	casher.findAll(function(err, rows, fields) {
		if(err) throw err;
		res.json(rows);
	})
})

app.post('/', function(req, res) {
	casher.register(req,res)			
})

// app.get('/', function(req, res) {
// 	casher.findByName(req,res) 
// })


module.exports = app

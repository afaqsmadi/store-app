var express = require('express');
var app = express();

// Import transactions Module
var transactions  = require('../models/transactions');

// API Routes
app.get('/', function(req, res) {

	transactions.findAll(function(err, rows, fields) {
		if(err) throw err;
		res.json(rows);
	})
})
app.post('/', function(req, res) {
	transactions.addTransactions(req,res)			
})

module.exports = app
